# Taller 02

**Importante:**
Para resolver los siguientes ejercicios debe crearse una cuenta en la
plataforma [Hackerrank.](https://www.hackerrank.com)

<br>
<br>

## Ejercicios
[Ejercicios de SQL](https://www.hackerrank.com/domains/sql?badge_type=sql)

<br>

### Guia:
[Guia](https://diegorestrepoleal.blogspot.com/2022/05/sql-challenges-solved.html)

<br>

### Documentación:
[w3schools SQL](https://www.w3schools.com/sql/)
