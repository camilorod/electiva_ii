def fibon(n):
    a = 0
    b = 1

    for i in range(n):
        yield a
        a, b = b, a + b


def run():
    for x in fibon(1000000):
        print(x)
        print('\n')


if __name__ == '__main__':
    run()
