from milibreria import cuadrado


def run():
    x = float(input('Ingrese un número decimal: '))

    x_square = cuadrado(x)

    print(f'\n{x_square} es el cuadrado de {x}\n')


if __name__ == '__main__':
    run()
