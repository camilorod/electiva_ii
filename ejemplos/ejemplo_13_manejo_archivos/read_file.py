def leer_archivo():
    with open('numeros.txt', 'r') as f:
        print(f.read())


def run():
    leer_archivo()


if __name__ == '__main__':
    run()
