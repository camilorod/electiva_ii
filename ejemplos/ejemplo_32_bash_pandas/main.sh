#!/bin/bash


clear


./organizar_datos.sh


directorios=`ls -d */`

for carpeta in $directorios
do
    echo
    echo "Entrar a $carpeta"
    echo

    dataset=$(echo $carpeta | tr '[:upper:]' '[:lower:]' | sed 's/.$//')

    cp unir_datos.sh $carpeta
    cd $carpeta
    ./unir_datos.sh $dataset
    cd ..
done


exit 0
