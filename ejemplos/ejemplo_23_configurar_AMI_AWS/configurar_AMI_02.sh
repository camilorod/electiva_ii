#!/bin/bash

# * ------------------------------------------------------------------------------------
# * "THE BEER-WARE LICENSE" (Revision 42):
# * <diegorestrepoleal@gmail.com> wrote this file. As long as you retain this notice you
# * can do whatever you want with this stuff. If we meet some day, and you think
# * this stuff is worth it, you can buy me a beer in return Diego Andrés Restrepo Leal.
# * ------------------------------------------------------------------------------------

# Limpiar pantalla
clear

# Color de los mensajes
AZUL=$(tput setaf 6)
VERDE=$(tput setaf 2)
ROJO=$(tput setaf 1)
LILA=$(tput setaf 5)
LIMPIAR=$(tput sgr 0)

echo
echo "$VERDE ============= CONTINUACIÓN... ============= $LIMPIAR"
echo

echo
echo "$LILA consultar: https://docs.aws.amazon.com/es_es/AWSEC2/latest/UserGuide/ec2-lamp-amazon-linux-2.html $LIMPIAR"
echo

echo
echo "$AZUL Cambiar propiedad de grupo de /var/ww $LIMPIAR"
echo
sudo chown -R ec2-user:apache /var/www

echo
echo "$AZUL Permisos de escritura de grupo $LIMPIAR"
echo
sudo chmod 2775 /var/www && find /var/www -type d -exec sudo chmod 2775 {} \;
find /var/www -type f -exec sudo chmod 0664 {} \;

echo
echo "$AZUL MariaDB $LIMPIAR"
echo
sudo systemctl start mariadb
sudo mysql_secure_installation
sudo systemctl enable mariadb

exit 0
