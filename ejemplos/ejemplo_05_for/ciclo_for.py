print('\nEjemplo 1\n')

for i in range(10):
    print(i)


print('\nEjemplo 2\n')

for i in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]:
    print(i)


print('\nEjemplo 3\n')

for i in range(2, 21):
    print(i)


print('\nEjemplo 4\n')

for i in range(2, 21, 4):
    print(i)


print('\nEjemplo 5\n')

for i in 'Universidad Cooperativa de Colombia':
    print(i)


print('\nEjemplo 6\n')

for i in ['Santa Marta', 'Magdalena', 'Colombia']:
    print(i)
